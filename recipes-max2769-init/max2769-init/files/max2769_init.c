// Copyright 2020 Christian Haettich <feddischson@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <linux/spi/spidev.h>
#include <array>
#include <cstdint>
#include <iostream>
#include <string>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <unistd.h>
using namespace std;

static constexpr int N_CONF = 3;
static constexpr int N_REG = 10;

//  This small Linux tool writes one of the following configurations
//  via SPI to the MAX2769 chip (assuming that the SPI device
//  and the MAX2769 chip is wired correctly).

static constexpr array<array<uint32_t, N_REG>, N_CONF> MAX2769_CONF = {{
    // Wideband glonass setup
    //   GNSSR_CLK: 32736000 Hz
    //   FI:        5097000 Hz
    //   FS:        32736000 Hz
    {
        0xa29b019,
        0x855028c,
        0xeaff1dc,
        0x98c0008,
        0x0c32080,
        0x8000070,
        0x8000000,
        0x8008002,
        0x1e0f401,
        0x28c0402,
    },
    // Wideband GPS/Galileo setup
    //   GNSSR_CLK: 32736000 Hz
    //   FI:        2046000 Hz
    //   FS:        32736000 Hz
    {
        0xa29b019,
        0x855028c,
        0xeaff1dc,
        0x98c0008,
        0x0c04080,
        0x8000070,
        0x8000000,
        0x8008002,
        0x1e0f401,
        0x28c0402,
    },
    // Galileo/GPS narrow-band setup
    //   GNSS-CLK:  32736000 Hz
    //   FI:        4092000 Hz
    //   FS:        16368000 Hz
    {
        0xa293573,
        0x855028c,
        0xeaff1dc,
        0x98c0008,
        0x0c00080,
        0x8000070,
        0x8000000,
        0x800800a,
        0x1e0f401,
        0x28c0402,
    },
}};

int write_max2769_word(int fd, uint8_t addr, uint32_t value) {
  /* 4 bit address */
  addr = addr & 0xf;
  value = (value << 4) | addr;

  std::cout << static_cast<int>(addr) << ": " << std::hex << value << std::endl;

  static uint32_t speed = 10000;
  uint8_t tx[] = {static_cast<uint8_t>((value >> 24) & 0xff),
                  static_cast<uint8_t>((value >> 16) & 0xff),
                  static_cast<uint8_t>((value >> 8) & 0xff),
                  static_cast<uint8_t>((value >> 0) & 0xff)};
  uint8_t rx[4];
  int ret;
  ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
  if (ret == -1) {
    cerr << "Can't set spi speed" << endl;
    return -1;
  }

  struct spi_ioc_transfer tr;

  tr.tx_buf = reinterpret_cast<long unsigned>(tx);
  tr.rx_buf = reinterpret_cast<long unsigned>(rx);
  tr.len = 4;
  tr.delay_usecs = 20;
  tr.speed_hz = speed;
  tr.bits_per_word = 8;
  tr.cs_change = 0;
  tr.tx_nbits = 0;
  tr.rx_nbits = 0;
  tr.pad = 0;

  ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
  if (ret < 1) {
    cerr << "SPI write error" << endl;
    return -1;
  }
  return 0;
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    cout << "Invalid number of arguments. Usage:" << endl
         << "  max2769_init <spi-path> <sel>" << endl
         << "        <spi-path>: Path to spi-device" << endl
         << "        <sel>:      0 = Wideband glonass setup" << endl
         << "                    1 = Wideband GPS/Galileo setup" << endl
         << "                    2 = Galileo/GPS narrow-band setup" << endl
         << endl;
    exit(-1);
  }

  int spi_fd;
  int conf_sel = stoi(string(argv[2]));
  if (conf_sel < 0 || conf_sel > 2) {
    cerr << "Error <sel> must be >= 0 and <= 2!" << endl;
    exit(-1);
  }

  spi_fd = open(argv[1], O_RDWR);
  if (spi_fd < 1) {
    cerr << "Failed to open SPI device " << argv[2] << endl;
    exit(-1);
  }

  bool ok = true;
  for (int ii = 0; ii < N_REG; ++ii) {
    int ret = write_max2769_word(spi_fd, ii, MAX2769_CONF[conf_sel][ii]);
    if (ret < 0) {
      ok = false;
      break;
    }
  }
  close(spi_fd);
  if (ok) {
    exit(0);
  } else {
    exit(-1);
  }
}
