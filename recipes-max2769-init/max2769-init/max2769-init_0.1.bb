SUMMARY = "Small initialization tool for the MAX2769 chip."
SECTION = "base"
LICENSE = "MIT"
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "\
      file://max2769_init.c \
	   "

do_compile () {
	${CXX} ${CFLAGS} ${LDFLAGS} ${WORKDIR}/max2769_init.c -o ${WORKDIR}/max2769_init
}

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/max2769_init ${D}${bindir}/
}

